FROM gliderlabs/herokuish:master as builder
COPY . /tmp/app
ARG BUILDPACK_URL
ENV USER=herokuishuser
RUN /bin/herokuish buildpack build

FROM gliderlabs/herokuish

COPY --chown=herokuishuser:herokuishuser --from=builder /app /app

ENV PATH $PATH:/app/.heroku/php/bin
RUN cd /app/ && composer run-script post-root-package-install
RUN chmod 0755 /app/sheduler.sh

ENV USER=herokuishuser

ARG APP_PORT=5000
ENV PORT ${APP_PORT}
ENV APP_PORT ${APP_PORT}

# Laravel settings
ARG APP_SECURE="true"
ARG APP_ENV="local"
ARG APP_DEBUG="true"
ARG DB_DATABASE="database"
ARG DB_CONNECTION="mysql"
ARG LOG_CHANNEL="stderr"

ENV APP_ENV ${APP_ENV}
ENV APP_DEBUG ${APP_DEBUG}
ENV APP_SECURE ${APP_SECURE}
ENV DB_DATABASE ${DB_DATABASE}
ENV DB_CONNECTION ${DB_CONNECTION}
ENV LOG_CHANNEL ${LOG_CHANNEL}

EXPOSE ${APP_PORT}

CMD (nohup /app/sheduler.sh &); /bin/herokuish procfile start web
