# Laravel TPL Files

Это шаблонные файлы, которые мы используем для развертки приложений (Laravel) в нашем кластере

Здесь уже включены (и настроены) эти сервисы:
* Postgresql (включен)
* Redis (выключен)

И пакеты:
* Laravel Passport - https://laravel.com/docs/6.x/passport
* Laravel Telescope - https://laravel.com/docs/6.x/telescope
* WebTinker - https://github.com/spatie/laravel-web-tinker
* ApiDoc - https://laravel-apidoc-generator.readthedocs.io
* Laravel IDE Helper - https://github.com/barryvdh/laravel-ide-helper

## Вид Git для работы кластера:

### Master
Ваша продакшен ветка. Используйте с умом.

### Staging
Ваша препродакшен ветка. В ней вы должны тестировать все, что идет на прод. Здесь все еще включены отладочные тесты, но это будет исправлено к следующим версиям данного проекта.

### Любая другая ваша ветка
В них вы должны вести основную разработку.

## Как должна происходить миграция кода:
1. Сначала вы работаете в минорных ветках
2. Отправляете pull-request в staging
3. Тестируете все изменения в staging
4. Отправляете pull-request в master
5. Принудительно запускаете (или зовете того, кто может запустить) `deploy` операцию в ci/cd, для выкатки в продакшен.
